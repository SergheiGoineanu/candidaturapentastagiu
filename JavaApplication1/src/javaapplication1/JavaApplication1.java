package javaapplication1;

import java.io.*;
import java.util.*;

public class JavaApplication1 {

    public static void main(String[] args) {
        // TODO code application logic here
        int N; //lungimea secventei
        int max; //valoare utilizata pt cuantificarea sirului de valori
        int maxb = 0; // blocul de dimensiune maxima
        int k = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter N:");
        N = sc.nextInt();
        int[] vector = new int[N]; // vectorul asociat secventei
        int[] Blocks = new int[N - 1]; // vectorul asociat unui sir cu dimensiunile blocurilor
        System.out.println("Sequence:");
        for (int i = 0; i < N; i++) { //citesc secventa
            vector[i] = sc.nextInt();
        }
        max = vector[0];
        for (int i = 0; i < N - 1; i++) { //calculez numarul de blocuri si valorile acestora
            if (max > vector[i + 1]) {
                Blocks[k] += (max - vector[i + 1]); 
            } else {
                max = vector[i + 1];
                k++;

            }

        }
        for (int i = 0; i < k; i++) { //selectez blocul cu cele mai multe unitati

            if (maxb < Blocks[i]) { 
                maxb = Blocks[i];
            }
        }
        System.out.println("The biggest block of water would contain " + maxb + " units of water");
    }
}
